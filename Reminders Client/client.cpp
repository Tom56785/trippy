/*
Original Work Copyright (c) 2015-2016, Tom Albans <albans.thomas@gmail.com>
Modified Work Copyright: Nathan Davis (c) 2016 <eddiebeast97@aol.com>

This file is part of Trippy.

Trippy is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Trippy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Trippy.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "reminders.h"

int single_shot(int argc, char **argv)
{

    // all 3 parameters were sent to the program, send them all in one big bulk
    int bytecount = 0;
    string data = "";
    if (argc == 4 || argc == 5) {
        // there are 4 arguments, the first one potentially being the MD5 hash and one for each element of the reminder
        // combine and separate the data using pipeline '|' characters
        unsigned int x = (argc == 5) ? 2 : 1;
        for (; x < argc; x++) {
            data += argv[x];
            if (x != (argc - 1)) {
                data += "|";
            }
        }
        // add the ADD REMINDER instruction to the start
        data = (argc == 5) ? ((string)argv[1] + "|" + "A" + data) : ("A" + data);
    } else {
        // an invalid amount of arguments were passed
        cout << "An incorrect amount of arguments were passed!" << endl;
        return 1;
    }

    int sockfd = 0;

    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        cout << "Error: Could not create socket" << endl;
        close(sockfd);
        return 1;
    }

    // send the data in one bulk to the server
    if (send_data(sockfd, data) != 0) {
        // the data could not be sent to the server
        // no need to output an error as the function does so already
        return 1;
    }

    char *recBuf = new char[4097];
    memset(recBuf, 0, 4097);

    if ((bytecount = recv(sockfd, recBuf, 4096, 0)) <= 0) {
        cout << "The connection to the server failed!" << endl;
        close(sockfd);
        delete[] recBuf;
        return 1;
    }

    cout << recBuf << endl;

    delete[] recBuf;
    close(sockfd);
    return 0;
}

int send_data(int sock, string data)
{
    struct sockaddr_in serv_addr;

    get_ip(address);

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(rem_port);
    serv_addr.sin_addr.s_addr = inet_addr(ip);

    if (connect(sock, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0) {
        cout << "Error: Couldn't connect to server";
        close(sock);
        return 1;
    }

    int bytecount = 0;

    data += "\0";

    // send the data in one bulk to the server
    if ((bytecount = send(sock, data.c_str(), data.length(), 0)) == -1) {
        cout << "The data couldn't be sent to the server." << endl;
        close(sock);
        return 1;
    }
    return 0;
}

void synchronise()
{
    // this function is run in a separate thread that synchronises any reminders left in the queue and retrieves any new reminders
    while (true) {
        pause_sync();
        if (local) {
            // the usage of the server is not required and a local file is being used
            continue;
        }

        mtxSync.lock();
        // check if there are any reminders in the queue, if there is then attempt to synchronise them with the server
        while (!rem_queue.empty()) {
            // there are reminders in the queue so attmept to synchronise them all
            // communicate with the server and send the data across
            string data = rem_queue.front();

            int sock = 0;

            if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
                cout << "Error: Could not create socket";
                close(sock);
                continue;
            }
            // attempt to send the instruction to the server
            if (send_data(sock, data)) {
                // the reminder couldn't be sent to the server so break from the loop
                break;
            }
            close(sock);
            // the reminder was successfully sent to the server, remove that reminder from the queue
            rem_queue.pop();
        }

        if (!rem_queue.empty()) {
            // some reminders weren't synchronised therefore connection to the server failed, so start the loop again
            continue;
        }
        mtxSync.unlock();

        // now attempt to synchronise with the server
        // all reminders received from the server are all the current and updated reminders
        int bytecount = 0;
        int sockfd = 0;

        if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
            cout << "Error: Could not create socket";
            close(sockfd);
            continue;
        }

        string temp = (md5_hash.empty() == false) ? (md5_hash + "|" + "SYNC") : "SYNC";
        if (send_data(sockfd, temp) != 0) {
            cout << "The data couldn't be sent to the server." << endl;
            continue;
        }

        // the SYNC command was successfully sent to the server, so retrieve all its data
        // empty the reminders vector and the listbox ready for the new data
        string data = "";
        mtxSync.lock();
        reminders.clear();
        mtxSync.unlock();
        gdk_threads_add_idle(rem_clear_tree, 0);

        char *recBuf = new char[4097];
        memset(recBuf, 0, 4097);

        while (true) {
            while ((bytecount = recv(sockfd, recBuf, 4096, 0)) > 0) {
                data += recBuf;
                memset(recBuf, 0, 4097); // clear the receiving buffer
            }

            if (data == "") {
                break;
            } else if (data == "INCORRECT_PASSWORD") {
                // the password sent to the server is incorrect
                cout << "The password is incorrect for the server!" << endl;
                break;
            }

            while (data != "") {

                reminder temp;
                // all the reminders have been sent all at once
                // each reminder is split up into segments and handled individually
                string segment;
                int seg_i = data.find((char)30);
                if (seg_i >= 0 && seg_i < data.length()) {
                    segment = data.substr(0, seg_i);
                    data.erase(0, seg_i + 1);
                }

                int i = segment.find("|");
                temp.title = segment.substr(0, i);
                segment.erase(0, i + 1);

                if (segment.substr(0, 4) != "null") {
                    i = segment.find("/");
                    temp.date.tm_mday = to_int(segment.substr(0, i));
                    segment.erase(0, i + 1);
                    i = segment.find("/");
                    temp.date.tm_mon = to_int(segment.substr(0, i)) - 1;
                    segment.erase(0, i + 1);
                    temp.date.tm_year = to_int(segment.substr(0, 4)) - 1900;
                    segment.erase(0, segment.find(" ") + 1);

                    i = segment.find(":");
                    temp.date.tm_hour = to_int(segment.substr(0, i));
                    segment.erase(0, i + 1);
                    i = segment.find("|");
                    temp.date.tm_min = to_int(segment.substr(0, i));
                    segment.erase(0, i + 1);
                } else {
                    segment.erase(0, segment.find("|") + 1);
                }

                if (segment == "null" || segment == "null\n") {
                    temp.notes = "";
                } else {
                    temp.notes = segment;
                }
                mtxSync.lock();
                reminders.push_back(temp);
                mtxSync.unlock();
            }
        }
        close(sockfd);
        delete[] recBuf;

        // add all the items into the listbox all at once obtaining only one thread lock
        gdk_threads_add_idle(rem_add_items, 0);
    }
}

int rem_clear_tree(gpointer data)
{
    gtk_tree_store_clear(GTK_TREE_STORE(listboxstore));
    return G_SOURCE_REMOVE;
}

int rem_add_items(gpointer data)
{
    for (unsigned int x = 0; x < reminders.size(); x++) {
        add_list_item(reminders[x].title.c_str());
    }
    return G_SOURCE_REMOVE;
}

void time_passed()
{
    // this function runs in a separate thread and checks if any reminder times have passed
    while (true) {
        pause(2000); // check if any reminders have passed every 2 seconds
        // first get the current time and the reminder time to compare it to
        time_t current = time(0);
        mtxSync.lock();
        for (unsigned int x = 0; x < reminders.size(); x++) {
            if (reminders[x].date.tm_year <= 0) {
                // either no reminder was set or the user was already reminded of this reminder, skip it
                continue;
            }
            tm temp = reminders[x].date;
            time_t comp = mktime(&temp);
            double diff = difftime(current, comp);
            // if the result is positive then the reminder time has passed
            // the current time in seconds is greater than the reminder time in seconds
            if (diff > 0) {
                // the reminder has passed

                // get all the data from the current reminder and synchronise it with the server
                reminders[x].date = tm();
                string data = "";
                data += reminders[x].title + "|" + reminders[x].title;
                data += "|null|";
                if (reminders[x].notes.empty()) {
                    data += "null";
                } else {
                    data += reminders[x].notes;
                }

                // add the EDIT REMINDER instruction to the start
                data = "E" + data;

                // send the reminder to the server
                int sockfd = 0;
                int bytecount = 0;

                if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
                    cout << "Error: Could not create socket";
                    continue;
                }

                if (send_data(sockfd, data)) {
                    // add the reminder into the queue to be synchronised with the server
                    rem_queue.push(data);
                }

                char *recBuf = new char[4097];
                memset(recBuf, 0, 4097);

                if ((bytecount = recv(sockfd, recBuf, 4096, 0)) <= 0) {
                    cout << "The connection to the server failed!" << endl;
                }

                close(sockfd);
                delete[] recBuf;

                gdk_threads_add_idle(reminder_passed, (void*)(reminders[x].title.c_str()));
            }
        }
        mtxSync.unlock();
    }
}

int reminder_passed(gpointer data)
{
    // set the reminder alarm details back to their defaults if that reminder was selected
    if (string(gtk_entry_get_text(GTK_ENTRY(noteRemtxtReminder))) == string((char*)data)) {
        time_t t = time(0);
        tm *curr = localtime(&t);
        gtk_combo_box_set_active(GTK_COMBO_BOX(noteRemcboDD), curr->tm_mday - 1);
        gtk_combo_box_set_active(GTK_COMBO_BOX(noteRemcboMM), curr->tm_mon);
        gtk_combo_box_set_active(GTK_COMBO_BOX(noteRemcboYYYY), 0);
        gtk_spin_button_set_value(GTK_SPIN_BUTTON(noteRemspinHH), 0);
        gtk_spin_button_set_value(GTK_SPIN_BUTTON(noteRemspinMM), 0);
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(noteRemchkDate), false);
        toggle_date_time(0);
    }
    // edit the date/time of the reminder so it isn't shown again at next sync cycle
    mtxSync.lock();
    unsigned int x = 0;
    for (; x < reminders.size(); x++) {
        if (reminders[x].title == (char*)data) {
            // the reminder in the vector was found
            // reset the date/time structure
            break;
        }
    }
    reminders[x].date = tm();
    mtxSync.unlock();
    // if a local file is used then write to it, if not then send the edit command to the server for the reminder
    if (local) {
        // write all the reminders to the local file
        write_rem();
    } else {
        // get all the data for the reminder and send the edit command
        string temp = (md5_hash.empty() == false) ? (md5_hash + "|" + "E") : ("E");

        // add the old name and new name into the data variable
        temp += reminders[x].title + "|" + reminders[x].title;

        // add 'null' for the new date/time
        temp += "|null|";

        // add the notes into the data variable
        temp += reminders[x].notes;


        // send the reminder to the server
        int sockfd = 0;
        int bytecount = 0;

        char *recBuf = new char[4097];
        memset(recBuf, 0, 4097);

        if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
            cout << "Error: Could not create socket" << endl;
            close(sockfd);
            return G_SOURCE_REMOVE;
        }

        if (send_data(sockfd, temp) || (bytecount = recv(sockfd, recBuf, 4096, 0)) <= 0) {
            message_box(((string)recBuf == "INCORRECT_PASSWORD") ? "The password is incorrect!" : "The reminder could not be changed on the server!", "Error!", GTK_BUTTONS_OK, GTK_MESSAGE_ERROR);
            // add the reminder into the queue to be synchronised with the server
            mtxSync.lock();
            rem_queue.push(temp);
            mtxSync.unlock();
        }

        delete[] recBuf;
        close(sockfd);
     }

    // show the window and a message box to remind the user of the reminder
    message_box((char*)data, "Reminder!", GTK_BUTTONS_OK, GTK_MESSAGE_INFO);
    return G_SOURCE_REMOVE;
}

void write_rem()
{
    ofstream file(rem_path);
    if (file.fail()) {
        // the file could not be opened
        file.close();
        message_box("The file could not be written to! Check folder permissions!", "Error!", GTK_BUTTONS_OK, GTK_MESSAGE_ERROR);
        return;
    }

    mtxSync.lock();
    for (unsigned int x = 0; x < reminders.size(); x++) {
        file << "{\n";
        file << reminders[x].title << "\n";

        string date = "";
        if (reminders[x].date.tm_year > 0) {
            date += std::to_string(reminders[x].date.tm_mday);
            date += "/";
            date += std::to_string(reminders[x].date.tm_mon + 1);
            date += "/";
            date += std::to_string(reminders[x].date.tm_year + 1900);
            date += " ";
            date += std::to_string(reminders[x].date.tm_hour);
            date += ":";
            date += std::to_string(reminders[x].date.tm_min);
        } else {
            date = "null";
        }

        file << date << "\n";
        string notes;
        if (reminders[x].notes != "null") {
            notes = reminders[x].notes;
        } else {
            notes = "null";
        }
        file << notes << "\n";
        file << "}\n";
    }
    file.close();
    mtxSync.unlock();
}

void read_rem()
{
    // read in the reminders file and add any currently existing reminders into the vector
    ifstream input(rem_path);
    if (input.fail()) {
        // the file could not be opened
        input.close();
        return;
    }

    vector<string> data;
    while (!input.eof()) {
        string temp;
        getline(input, temp);
        data.push_back(temp);
    }
    mtxSync.lock();
    reminders.clear();
    mtxSync.unlock();
    // loop through the vector until an opening bracket is found
    for (unsigned int x = 0; x < data.size(); x++) {
        reminder temp;
        if (data[x] == "{") {
            // the first line is the reminder title
            x++;
            temp.title = data[x];
            // the second line is the date/time
            x++;
            if (data[x] != "null") {
                int i = data[x].find("/");
                temp.date.tm_mday = to_int(data[x].substr(0, i));
                data[x].erase(0, i + 1);
                i = data[x].find("/");
                temp.date.tm_mon = to_int(data[x].substr(0, i)) - 1;
                data[x].erase(0, i + 1);
                temp.date.tm_year = to_int(data[x].substr(0, 4)) - 1900;
                data[x].erase(0, data[x].find(" ") + 1);

                i = data[x].find(":", i + 1);
                temp.date.tm_hour = to_int(data[x].substr(0, i));
                data[x].erase(0, i + 1);
                temp.date.tm_min = to_int(data[x]);
            }

            // the rest of the lines up until the close bracket are notes
            x++;
            string notes = "";
            while (true) {
                if (data[x] == "}") {
                    // the final line of the reminder has been reached so break
                    break;
                }
                // if the last line of the notes is reached then don't add any trailing new lines
                if (data[x + 1] == "}") {
                    notes += data[x];
                } else {
                    notes += data[x] + "\n";
                }
                x++;
            }
            temp.notes = notes;
            add_list_item(temp.title.c_str());
            // add the reminder into the vector, then check for another reminder
            mtxSync.lock();
            reminders.push_back(temp);
            mtxSync.unlock();
        }
    }
    input.close();
}

void write_config()
{
    // this function writes the data into the configuration file
    mtxSync.lock();

    ofstream write("config.cfg");
    if (write.fail()) {
        write.close();
        message_box("The configuration file could not be written!", "Error!", GTK_BUTTONS_OK, GTK_MESSAGE_ERROR);
        mtxSync.unlock();
        return;
    }
    write << "server = \"" << address << "\";\n";
    write << "port = " << rem_port << ";\n";
    write << "update = " << interval << ";\n";
    write << "background = " << ((background) ? "true;\n" : "false;\n");
    write << "local = " << ((local) ? "true;\n" : "false;\n");
    write << "hash = \"" << md5_hash << "\";";
    write.close();

    mtxSync.unlock();
}

void read_config()
{
    // this function reads the data in the configuration file and applies it
    mtxSync.lock();

    Config conf;

    try {
        // attempt to read in the config file
        conf.readFile("config.cfg");

        string server = conf.lookup("server");
        int port = conf.lookup("port");
        int update = conf.lookup("update");
        bool backgr = conf.lookup("background");
        bool loc = conf.lookup("local");
        string hash_data = conf.lookup("hash");

        // the data was successfully read so apply it
        if (!server.empty()) {
            address = server;
        }
        if (port > 0) {
            rem_port = port;
        }
        if (update > 0) {
            interval = update;
        }
        background = backgr;
        local = loc;
        md5_hash = hash_data;
    } catch (const ParseException &ex) {
        message_box("There was a configuration parse error!", "Error!", GTK_BUTTONS_OK, GTK_MESSAGE_ERROR);
    } catch (std::exception) {
        mtxSync.unlock();
        return;
    }
    mtxSync.unlock();
}

void pause(int duration)
{
    // sleep for the designated amount of time in milliseconds
    std::this_thread::sleep_for(std::chrono::milliseconds(duration));
    return;
}

void pause_sync()
{
    // sleep for the designated amount of time in milliseconds
    unsigned int x = 0;
    while (true) {
        //mtxSync.lock();
        if (x >= interval || sync_rem) {
            // the synchronise interval was reached or a synchronisation was requested
            sync_rem = false;
            return;
        }
        //mtxSync.unlock();
        std::this_thread::sleep_for(std::chrono::seconds(1));
        x++;
    }
    return;
}
